//
//  ViewController.swift
//  TimePlay
//
//  Created by Sourav Dewett on 2018-07-29.
//  Copyright © 2018 Sourav Dewett. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class ViewController: UIViewController {
    
    var ref:DatabaseReference!
    
    @IBOutlet weak var usertextbox: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ref = Database.database().reference()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func savetofb(_ sender: Any) {
        let user = usertextbox.text!
        
        if(user == "Admin" || user == "admin"){
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "adminSB") as? AdminViewController
            
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            let x = ["username":user, "score":0] as [String : Any]
            let databaseReference: DatabaseReference = self.ref.child("users").childByAutoId()
            let key = databaseReference.key
            print(databaseReference.key)
            databaseReference.setValue(x)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "userSB") as? UserViewController
            vc?.currentUser = user
            vc?.currentUserKey = key
            
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
}
