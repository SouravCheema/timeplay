//
//  Question.swift
//  TimePlay
//
//  Created by Sourav Dewett on 2018-07-29.
//  Copyright © 2018 Sourav Dewett. All rights reserved.
//

import Foundation
import Firebase

class QuestionModel {
    var AnswerA : String?
    var AnswerB : String?
    var AnswerC : String?
    var AnswerD : String?
    var CorrectAnswer : String?
    var Question : String?
    
    init(){
        self.AnswerA = ""
        self.AnswerB = ""
        self.AnswerC = ""
        self.AnswerD = ""
        self.CorrectAnswer = ""
        self.Question = ""
    }
    
    init(AnswerA: String, AnswerB: String, AnswerC: String, AnswerD: String, CorrectAnswer: String, Question: String) {
        self.AnswerA = AnswerA
        self.AnswerB = AnswerB
        self.AnswerC = AnswerC
        self.AnswerD = AnswerD
        self.CorrectAnswer = CorrectAnswer
        self.Question = Question
    }
}
