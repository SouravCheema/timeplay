//
//  UserViewController.swift
//  TimePlay
//
//  Created by Sourav Dewett on 2018-07-29.
//  Copyright © 2018 Sourav Dewett. All rights reserved.
//

import UIKit
import Firebase

class UserViewController: UIViewController {

    @IBOutlet var quizStopView: UIView!
    @IBOutlet var quizStartView: UIView!
    @IBOutlet var option1: UIButton!
    @IBOutlet var option2: UIButton!
    @IBOutlet var option3: UIButton!
    @IBOutlet var option4: UIButton!
    @IBOutlet var timer: UILabel!
    
    //Timer
    var countdownTimer: Timer!
    var totalTime = 10
    
    //Firebase
    var ref: DatabaseReference = Database.database().reference()
    
    var listOfQuestions = [QuestionModel]()
    var counter = 0;
    var counterFinal = 0;
    
    //This is being retrieved from previous screen
    var currentUser = ""
    var currentUserKey = ""
    
    //keeping track of the score of the user
    var score = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Loading all the questions, answers and correct option
        ref.child("Questions").observeSingleEvent(of: .value, with: {snapshot in
            
            print(snapshot.childrenCount) // I got the expected number of items
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? DataSnapshot {
                let value = rest.value as? NSDictionary
                
                let questionModel = QuestionModel(AnswerA: value?["AnswerA"] as! String, AnswerB: value?["AnswerB"] as! String, AnswerC: value?["AnswerC"] as! String, AnswerD: value?["AnswerD"] as! String, CorrectAnswer: value?["CorrectAnswer"] as! String, Question: value?["Question"] as! String)
                
                //Testing if it works or not
                print(questionModel.AnswerA!)
                
                //self.common.addToQuestionsList(question: questionModel)
                self.listOfQuestions.append(questionModel)
                print("List of questions count :")
                print(self.listOfQuestions.count)
            }
            
            print("Testing the size")
            print(self.listOfQuestions.count)
            self.counterFinal = self.listOfQuestions.count
        })

        // Checking if quiz is started by admin or not
        ref.child("quiz").observe(.childChanged, with: { (snapshot) in
            
            let dataKey = snapshot.key
            let dataValue = snapshot.value
            
            print(dataKey);
            print(dataValue);
            
            if(dataKey == "start"){
                if(dataValue as! Int == 0){
                    self.stopQuiz()
                }
                if(dataValue as! Int == 1){
                    self.startQuiz()
                }
            }
        })
        
        ref.child("users").child(currentUserKey).observe(.childChanged, with: { (snapshot) in
            let dataKey = snapshot.key
            let dataValue = snapshot.value
            
            print(dataKey);
            print(dataValue);
            
            if(dataKey == "score"){
            self.score = dataValue as! Int
            }
        })
    }
    
    func startQuiz(){
        self.quizStopView.isHidden = true
        self.quizStartView.isHidden = false
        
        showAnswer()
    }
    
    func stopQuiz(){
        self.quizStopView.isHidden = false
        self.quizStartView.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAnswer() {
        print("Counter value")
        print(counter)
        self.option1.setTitle(self.listOfQuestions[counter].AnswerA, for: UIControlState.normal)
        self.option2.setTitle(self.listOfQuestions[counter].AnswerB, for: UIControlState.normal)
        self.option3.setTitle(self.listOfQuestions[counter].AnswerC, for: UIControlState.normal)
        self.option4.setTitle(self.listOfQuestions[counter].AnswerD, for: UIControlState.normal)
        startTimer()
    }
    

    //-------------------------Timer-------------------------
    
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        timer.text = "\(timeFormatted(totalTime))"
        
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
        }
    }
    
    func endTimer() {
        countdownTimer.invalidate()
        if counter < counterFinal - 1 {
            self.totalTime = 10
            counter = counter + 1
            
            option1.isEnabled = true;
            option2.isEnabled = true;
            option3.isEnabled = true;
            option4.isEnabled = true;
            
            showAnswer()
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    //-------------------------Timer ends-------------------------
    
    
    @IBAction func Option1Clicked(_ sender: UIButton) {
        let correctAnswer = listOfQuestions[counter].CorrectAnswer
        if correctAnswer == "AnswerA" {
            let x = ["username": currentUser, "score":score+10] as [String : Any]
            self.ref.child("users").child(currentUserKey).setValue(x)
        }
        
        option1.isEnabled = false;
        option2.isEnabled = false;
        option3.isEnabled = false;
        option4.isEnabled = false;
    }
    
    @IBAction func Option2Clicked(_ sender: UIButton) {
        let correctAnswer = listOfQuestions[counter].CorrectAnswer
        if correctAnswer == "AnswerB" {
            let x = ["username": currentUser, "score":score+10] as [String : Any]
            self.ref.child("users").child(currentUserKey).setValue(x)
        }
        
        option1.isEnabled = false;
        option2.isEnabled = false;
        option3.isEnabled = false;
        option4.isEnabled = false;
    }
    
    
    @IBAction func Option3Clicked(_ sender: UIButton) {
        let correctAnswer = listOfQuestions[counter].CorrectAnswer
        if correctAnswer == "AnswerC" {
            let x = ["username": currentUser, "score":score+10] as [String : Any]
            self.ref.child("users").child(currentUserKey).setValue(x)
        }
        
        option1.isEnabled = false;
        option2.isEnabled = false;
        option3.isEnabled = false;
        option4.isEnabled = false;
    }
    
    
    @IBAction func Option4Clicked(_ sender: UIButton) {
        let correctAnswer = listOfQuestions[counter].CorrectAnswer
        if correctAnswer == "AnswerD" {
            let x = ["username": currentUser, "score":score+10] as [String : Any]
            self.ref.child("users").child(currentUserKey).setValue(x)
        }
        
        option1.isEnabled = false;
        option2.isEnabled = false;
        option3.isEnabled = false;
        option4.isEnabled = false;
    }
    
    
}
