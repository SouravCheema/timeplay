//
//  AdminViewController.swift
//  TimePlay
//
//  Created by Sourav Dewett on 2018-07-29.
//  Copyright © 2018 Sourav Dewett. All rights reserved.
//

import UIKit
import Firebase

class AdminViewController: UIViewController {

    @IBOutlet var quizStoppedView: UIView!
    
    @IBOutlet var quizStartView: UIView!
    
    //Result
    @IBOutlet var resultView: UIView!
    
    @IBOutlet var quizStartButton: UIButton!
    @IBOutlet var quizStopButton: UIButton!
    
    
    //QuizQuestion
    @IBOutlet var timer: UILabel!
    @IBOutlet var question: UILabel!
    
    //Result
    @IBOutlet var winnerName: UILabel!
    @IBOutlet var winnerScore: UILabel!
    
    var score = 0
    var winner = ""
    
    
    //Firebase
    var ref: DatabaseReference = Database.database().reference()
    
    //Timer
    var countdownTimer: Timer!
    var totalTime = 10
    
    var listOfQuestions = [QuestionModel]()
    var counter = 0;
    var counterFinal = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        ref.child("Questions").observeSingleEvent(of: .value, with: {snapshot in
            
            print(snapshot.childrenCount) // I got the expected number of items
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? DataSnapshot {
                let value = rest.value as? NSDictionary
                
                let questionModel = QuestionModel(AnswerA: value?["AnswerA"] as! String, AnswerB: value?["AnswerB"] as! String, AnswerC: value?["AnswerC"] as! String, AnswerD: value?["AnswerD"] as! String, CorrectAnswer: value?["CorrectAnswer"] as! String, Question: value?["Question"] as! String)
                
                //Testing if it works or not
                print(questionModel.AnswerA!)
                
                //self.common.addToQuestionsList(question: questionModel)
                self.listOfQuestions.append(questionModel)
                print("List of questions count :")
                print(self.listOfQuestions.count)
            }
            
            print("Testing the size")
            print(self.listOfQuestions.count)
            self.counterFinal = self.listOfQuestions.count
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Admin"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startQuiz(_ sender: UIButton) {
        quizStartView.isHidden = false;
        quizStoppedView.isHidden = true;
        resultView.isHidden = true
        quizStartButton.isEnabled = false;
        quizStopButton.isEnabled = true;
        
        updateQuizStart()
        showQuestion()
    }
    
    @IBAction func stopQuiz(_ sender: UIButton) {
        quizStartView.isHidden = true;
        quizStoppedView.isHidden = false;
        resultView.isHidden = true
        quizStartButton.isEnabled = true;
        quizStopButton.isEnabled = false;
        updateQuizStop()
        countdownTimer.invalidate()
    }
    
    func showQuestion(){
        print("Counter value")
        print(counter)
        self.question.text = self.listOfQuestions[counter].Question
        startTimer()
    }
    
    //-------------------------Timer-------------------------
    
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        timer.text = "\(timeFormatted(totalTime))"
        
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
        }
    }
    
    func endTimer() {
        countdownTimer.invalidate()
        if counter < counterFinal - 1 {
            self.totalTime = 10
            counter = counter + 1
            showQuestion()
        } else {
            showResult();
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    //-------------------------Timer ends-------------------------
    
    func updateQuizStop(){
        let prntRef  = ref.child("quiz")
        prntRef.updateChildValues(["start":0])
    }
    
    func updateQuizStart(){
        let prntRef  = ref.child("quiz")
        prntRef.updateChildValues(["start":1])
    }
    
    func showResult(){
        
        quizStartView.isHidden = true
        quizStoppedView.isHidden = true
        resultView.isHidden = false
        
        ref.child("users").observeSingleEvent(of: .value, with: { (snapshot) in
            
            print(snapshot.childrenCount) // I got the expected number of items
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? DataSnapshot {
                let value = rest.value as? NSDictionary
            
                let s = value?["score"] as! Int!
                let u = value?["username"] as! String!
                
                if s! > self.score {
                    self.winnerName.text = u
                    self.winnerScore.text  = String(s ?? 0)
                }
            }
            
        })
    }
    
    @IBAction func resetAll(_ sender: UIButton) {
        ref.child("users").removeValue()
        
        quizStartView.isHidden = true;
        quizStoppedView.isHidden = false;
        resultView.isHidden = true
        quizStartButton.isEnabled = true;
        quizStopButton.isEnabled = false;
        self.counter = 0
        updateQuizStop()
    }
}
